#
# Makefile
#

all:
		$(MAKE) -C tools all
		@echo "+------------------------------------------------+"
		@echo "|  Please follow the HOWTO to get things going!  |"
		@echo "+------------------------------------------------+"

clean:
		$(MAKE) -C tools clean
		$(MAKE) -C sim clean
		$(MAKE) -C mktape clean
		$(MAKE) -C run clean
		$(MAKE) -C dist clean
		$(MAKE) -C xfiles clean
		rm -f *~
