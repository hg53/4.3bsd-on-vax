/*
 * mktape.c
 */


#include <stdio.h>
#include <stdlib.h>


#define OUTPUT_FILE	"4.3BSD.tp"


struct {
  char *name;
  int blocksize;
} files[] = {
  { "stand",        512 },
  { "miniroot",   10240 },
  { "rootdump",   10240 },
  { "usr.tar",    10240 },
  { "srcsys.tar", 10240 },
  { "src.tar",    10240 },
  { "vfont.tar",  10240 },
  { "new.tar",    10240 },
  { "ingres.tar", 10240 },
};


void error(char *msg) {
  printf("**** Error: %s ****\n", msg);
  exit(0);
}


void writeCount(long count, FILE *file) {
  if (fwrite(&count, 4, 1, file) != 1) {
    error("cannot write byte count");
  }
}


int main(void) {
  FILE *outfile;
  FILE *infile;
  long size;
  int blocksize;
  int blocks;
  int i, j;
  unsigned char buffer[20000];

  outfile = fopen(OUTPUT_FILE, "wb");
  if (outfile == NULL) {
    error("cannot open output file");
  }
  for (i = 0; i < sizeof(files)/sizeof(files[0]); i++) {
    printf("%s: ", files[i].name);
    infile = fopen(files[i].name, "rb");
    if (infile == NULL) {
      error("cannot open input file");
    }
    fseek(infile, 0, SEEK_END);
    size = ftell(infile);
    fseek(infile, 0, SEEK_SET);
    blocksize = files[i].blocksize;
    blocks = size / blocksize;
    printf("%ld bytes = %d records (blocksize %d bytes)\n",
           size, blocks, blocksize);
    if (size % blocksize != 0) {
      error("file size is not a multiple of block size");
    }
    for (j = 0; j < blocks; j++) {
      if (fread(buffer, blocksize, 1, infile) != 1) {
        error("cannot read input file");
      }
      writeCount(blocksize, outfile);
      if (fwrite(buffer, blocksize, 1, outfile) != 1) {
        error("cannot write output file");
      }
      writeCount(blocksize, outfile);
    }
    fclose(infile);
    writeCount(0, outfile);
  }
  writeCount(0xFFFFFFFF, outfile);
  fclose(outfile);
  return 0;
}
