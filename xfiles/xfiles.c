/*
 * xfiles.c
 */


#include <stdio.h>
#include <stdlib.h>


struct {
  char *name;
  int blocksize;
} files[] = {
  { "stand",        512 },
  { "miniroot",   10240 },
  { "rootdump",   10240 },
  { "usr.tar",    10240 },
  { "srcsys.tar", 10240 },
  { "src.tar",    10240 },
  { "vfont.tar",  10240 },
  { "new.tar",    10240 },
  { "ingres.tar", 10240 },
};


void error(char *msg) {
  printf("**** Error: %s ****\n", msg);
  exit(1);
}


int readCount(FILE *file) {
  int count;

  if (fread(&count, 4, 1, file) != 1) {
    error("cannot read byte count");
  }
  return count;
}


int main(int argc, char *argv[]) {
  FILE *infile;
  FILE *outfile;
  int blocks;
  int blocksize;
  long size;
  int i;
  unsigned char buffer[20000];

  if (argc != 2) {
    printf("Usage: %s <input file>\n", argv[0]);
    return 1;
  }
  infile = fopen(argv[1], "rb");
  if (infile == NULL) {
    error("cannot open input file");
  }
  for (i = 0; i < sizeof(files)/sizeof(files[0]); i++) {
    printf("%s: ", files[i].name);
    outfile = fopen(files[i].name, "wb");
    if (outfile == NULL) {
      error("cannot open output file");
    }
    blocks = 0;
    while (1) {
      blocksize = readCount(infile);
      if (blocksize == 0) {
        /* file mark */
        break;
      }
      if (blocksize != files[i].blocksize) {
        error("unexpected leading block size");
      }
      if (fread(buffer, blocksize, 1, infile) != 1) {
        error("cannot read input file");
      }
      blocksize = readCount(infile);
      if (blocksize != files[i].blocksize) {
        error("unexpected trailing block size");
      }
      if (fwrite(buffer, blocksize, 1, outfile) != 1) {
        error("cannot write output file");
      }
      blocks++;
    }
    size = blocks * files[i].blocksize;
    printf("%d records (blocksize %d bytes) = %ld bytes\n",
           blocks, files[i].blocksize, size);
    fclose(outfile);
  }
  blocksize = readCount(infile);
  if (blocksize != 0 && blocksize != -1) {
    error("end-of-tape mark expected");
  }
  fclose(infile);
  return 0;
}
